
#pragma once

#include "matrix_types.hpp"

#include <cmath>
#include <iostream>

/// Assemble the residual for the network parameter time integration
inline auto assemble_residual(vector5 const& current_values,
                              vector5 const& previous_values,
                              double const p_s,
                              double const p_c,
                              double const t,
                              double const time_step_size) -> vector5
{
    auto const n_a = current_values(0);
    auto const n_ia = current_values(1);
    auto const N_a = current_values(2);
    auto const N_ia = current_values(3);
    auto const gamma = current_values(4);

    auto const n_a_old = previous_values(0);
    auto const n_ia_old = previous_values(1);
    auto const N_a_old = previous_values(2);
    auto const N_ia_old = previous_values(3);
    auto const gamma_old = previous_values(4);

    auto const active_segment_rate = (N_a - N_a_old) / time_step_size;
    auto const inactive_segment_rate = (N_ia - N_ia_old) / time_step_size;

    return (vector5{} << n_a - n_a_old
                             + time_step_size
                                   * (-2 * n_a * p_c * (N_a + t * active_segment_rate)
                                          * std::exp(-N_a * p_c * t)
                                      + n_a * p_s * (N_a + t * active_segment_rate)
                                            * std::exp(-N_a * p_s * t)
                                      - n_ia * p_c * (N_ia + t * inactive_segment_rate)
                                            * std::exp(-N_ia * p_c * t)),
            n_ia - n_ia_old
                - time_step_size
                      * (2 * n_a * p_s * (N_a + t * active_segment_rate) * std::exp(-N_a * p_s * t)
                         + n_ia
                               * (-2 * p_c * (N_ia + t * inactive_segment_rate)
                                      * std::exp(-N_ia * p_c * t)
                                  + p_s * (N_ia + t * inactive_segment_rate)
                                        * std::exp(-N_ia * p_s * t))),
            N_a - N_a_old
                - time_step_size
                      * (-N_a * n_a * p_s * (N_a + t * active_segment_rate) * std::exp(-N_a * p_s * t)
                         - N_a
                               * (n_a
                                      * (2 * p_c * (N_a + t * active_segment_rate)
                                             * std::exp(-N_a * p_c * t)
                                         - p_s * (N_a + t * active_segment_rate)
                                               * std::exp(-N_a * p_s * t))
                                  + n_ia * p_c * (N_ia + t * inactive_segment_rate)
                                        * std::exp(-N_ia * p_c * t))
                         + 2 * N_ia * n_ia * p_c * (N_ia + t * inactive_segment_rate)
                               * std::exp(-N_ia * p_c * t))
                      / n_a,
            N_ia - N_ia_old
                - time_step_size
                      * (N_a * n_a * p_s * (N_a + t * active_segment_rate) * std::exp(-N_a * p_s * t)
                         - 2 * N_ia * n_ia * p_c * (N_ia + t * inactive_segment_rate)
                               * std::exp(-N_ia * p_c * t)
                         - N_ia
                               * (2 * n_a * p_s * (N_a + t * active_segment_rate)
                                      * std::exp(-N_a * p_s * t)
                                  + n_ia
                                        * (-2 * p_c * (N_ia + t * inactive_segment_rate)
                                               * std::exp(-N_ia * p_c * t)
                                           + p_s * (N_ia + t * inactive_segment_rate)
                                                 * std::exp(-N_ia * p_s * t))))
                      / n_ia,
            gamma * time_step_size
                    * (2 * p_c * (N_a + t * active_segment_rate) * std::exp(-N_a * p_c * t)
                       + p_s * (N_a + t * active_segment_rate) * std::exp(-N_a * p_s * t))
                + gamma - gamma_old)
        .finished();
}

inline auto assemble_jacobian(vector5 const& current_values,
                              vector5 const& previous_values,
                              double const p_s,
                              double const p_c,
                              double const t,
                              double const time_step_size) -> matrix5
{
    auto const n_a = current_values(0);
    auto const n_ia = current_values(1);
    auto const N_a = current_values(2);
    auto const N_ia = current_values(3);
    auto const gamma = current_values(4);

    auto const N_a_old = previous_values(2);
    auto const N_ia_old = previous_values(3);

    auto const active_segment_rate = (N_a - N_a_old) / time_step_size;
    auto const inactive_segment_rate = (N_ia - N_ia_old) / time_step_size;

    return (matrix5{} << time_step_size
                                 * (-2 * p_c * (N_a + t * active_segment_rate)
                                        * std::exp(-N_a * p_c * t)
                                    + p_s * (N_a + t * active_segment_rate) * std::exp(-N_a * p_s * t))
                             + 1,
            -p_c * time_step_size * (N_ia + t * inactive_segment_rate) * std::exp(-N_ia * p_c * t),
            time_step_size
                * (2 * n_a * std::pow(p_c, 2) * t * (N_a + t * active_segment_rate)
                       * std::exp(-N_a * p_c * t)
                   - 2 * n_a * p_c * (t / time_step_size + 1) * std::exp(-N_a * p_c * t)
                   - n_a * std::pow(p_s, 2) * t * (N_a + t * active_segment_rate)
                         * std::exp(-N_a * p_s * t)
                   + n_a * p_s * (t / time_step_size + 1) * std::exp(-N_a * p_s * t)),
            time_step_size
                * (n_ia * std::pow(p_c, 2) * t * (N_ia + t * inactive_segment_rate)
                       * std::exp(-N_ia * p_c * t)
                   - n_ia * p_c * (t / time_step_size + 1) * std::exp(-N_ia * p_c * t)),
            0,
            -2 * p_s * time_step_size * (N_a + t * active_segment_rate) * std::exp(-N_a * p_s * t),
            -time_step_size
                    * (-2 * p_c * (N_ia + t * inactive_segment_rate) * std::exp(-N_ia * p_c * t)
                       + p_s * (N_ia + t * inactive_segment_rate) * std::exp(-N_ia * p_s * t))
                + 1,
            -time_step_size
                * (-2 * n_a * std::pow(p_s, 2) * t * (N_a + t * active_segment_rate)
                       * std::exp(-N_a * p_s * t)
                   + 2 * n_a * p_s * (t / time_step_size + 1) * std::exp(-N_a * p_s * t)),
            -n_ia * time_step_size
                * (2 * std::pow(p_c, 2) * t * (N_ia + t * inactive_segment_rate)
                       * std::exp(-N_ia * p_c * t)
                   - 2 * p_c * (t / time_step_size + 1) * std::exp(-N_ia * p_c * t)
                   - std::pow(p_s, 2) * t * (N_ia + t * inactive_segment_rate)
                         * std::exp(-N_ia * p_s * t)
                   + p_s * (t / time_step_size + 1) * std::exp(-N_ia * p_s * t)),
            0,
            -time_step_size
                    * (-N_a * p_s * (N_a + t * active_segment_rate) * std::exp(-N_a * p_s * t)
                       - N_a
                             * (2 * p_c * (N_a + t * active_segment_rate) * std::exp(-N_a * p_c * t)
                                - p_s * (N_a + t * active_segment_rate) * std::exp(-N_a * p_s * t)))
                    / n_a
                + time_step_size
                      * (-N_a * n_a * p_s * (N_a + t * active_segment_rate) * std::exp(-N_a * p_s * t)
                         - N_a
                               * (n_a
                                      * (2 * p_c * (N_a + t * active_segment_rate)
                                             * std::exp(-N_a * p_c * t)
                                         - p_s * (N_a + t * active_segment_rate)
                                               * std::exp(-N_a * p_s * t))
                                  + n_ia * p_c * (N_ia + t * inactive_segment_rate)
                                        * std::exp(-N_ia * p_c * t))
                         + 2 * N_ia * n_ia * p_c * (N_ia + t * inactive_segment_rate)
                               * std::exp(-N_ia * p_c * t))
                      / std::pow(n_a, 2),
            -time_step_size
                * (-N_a * p_c * (N_ia + t * inactive_segment_rate) * std::exp(-N_ia * p_c * t)
                   + 2 * N_ia * p_c * (N_ia + t * inactive_segment_rate) * std::exp(-N_ia * p_c * t))
                / n_a,
            1
                - time_step_size
                      * (N_a * n_a * std::pow(p_s, 2) * t * (N_a + t * active_segment_rate)
                             * std::exp(-N_a * p_s * t)
                         - N_a * n_a * p_s * (t / time_step_size + 1) * std::exp(-N_a * p_s * t)
                         - N_a * n_a
                               * (-2 * std::pow(p_c, 2) * t * (N_a + t * active_segment_rate)
                                      * std::exp(-N_a * p_c * t)
                                  + 2 * p_c * (t / time_step_size + 1) * std::exp(-N_a * p_c * t)
                                  + std::pow(p_s, 2) * t * (N_a + t * active_segment_rate)
                                        * std::exp(-N_a * p_s * t)
                                  - p_s * (t / time_step_size + 1) * std::exp(-N_a * p_s * t))
                         - n_a * p_s * (N_a + t * active_segment_rate) * std::exp(-N_a * p_s * t)
                         - n_a
                               * (2 * p_c * (N_a + t * active_segment_rate) * std::exp(-N_a * p_c * t)
                                  - p_s * (N_a + t * active_segment_rate) * std::exp(-N_a * p_s * t))
                         - n_ia * p_c * (N_ia + t * inactive_segment_rate) * std::exp(-N_ia * p_c * t))
                      / n_a,
            -time_step_size
                * (-N_a
                       * (-n_ia * std::pow(p_c, 2) * t * (N_ia + t * inactive_segment_rate)
                              * std::exp(-N_ia * p_c * t)
                          + n_ia * p_c * (t / time_step_size + 1) * std::exp(-N_ia * p_c * t))
                   - 2 * N_ia * n_ia * std::pow(p_c, 2) * t * (N_ia + t * inactive_segment_rate)
                         * std::exp(-N_ia * p_c * t)
                   + 2 * N_ia * n_ia * p_c * (t / time_step_size + 1) * std::exp(-N_ia * p_c * t)
                   + 2 * n_ia * p_c * (N_ia + t * inactive_segment_rate) * std::exp(-N_ia * p_c * t))
                / n_a,
            0,
            -time_step_size
                * (N_a * p_s * (N_a + t * active_segment_rate) * std::exp(-N_a * p_s * t)
                   - 2 * N_ia * p_s * (N_a + t * active_segment_rate) * std::exp(-N_a * p_s * t))
                / n_ia,
            -time_step_size
                    * (-2 * N_ia * p_c * (N_ia + t * inactive_segment_rate) * std::exp(-N_ia * p_c * t)
                       - N_ia
                             * (-2 * p_c * (N_ia + t * inactive_segment_rate)
                                    * std::exp(-N_ia * p_c * t)
                                + p_s * (N_ia + t * inactive_segment_rate) * std::exp(-N_ia * p_s * t)))
                    / n_ia
                + time_step_size
                      * (N_a * n_a * p_s * (N_a + t * active_segment_rate) * std::exp(-N_a * p_s * t)
                         - 2 * N_ia * n_ia * p_c * (N_ia + t * inactive_segment_rate)
                               * std::exp(-N_ia * p_c * t)
                         - N_ia
                               * (2 * n_a * p_s * (N_a + t * active_segment_rate)
                                      * std::exp(-N_a * p_s * t)
                                  + n_ia
                                        * (-2 * p_c * (N_ia + t * inactive_segment_rate)
                                               * std::exp(-N_ia * p_c * t)
                                           + p_s * (N_ia + t * inactive_segment_rate)
                                                 * std::exp(-N_ia * p_s * t))))
                      / std::pow(n_ia, 2),
            -time_step_size
                * (-N_a * n_a * std::pow(p_s, 2) * t * (N_a + t * active_segment_rate)
                       * std::exp(-N_a * p_s * t)
                   + N_a * n_a * p_s * (t / time_step_size + 1) * std::exp(-N_a * p_s * t)
                   - N_ia
                         * (-2 * n_a * std::pow(p_s, 2) * t * (N_a + t * active_segment_rate)
                                * std::exp(-N_a * p_s * t)
                            + 2 * n_a * p_s * (t / time_step_size + 1) * std::exp(-N_a * p_s * t))
                   + n_a * p_s * (N_a + t * active_segment_rate) * std::exp(-N_a * p_s * t))
                / n_ia,
            1
                - time_step_size
                      * (2 * N_ia * n_ia * std::pow(p_c, 2) * t * (N_ia + t * inactive_segment_rate)
                             * std::exp(-N_ia * p_c * t)
                         - 2 * N_ia * n_ia * p_c * (t / time_step_size + 1) * std::exp(-N_ia * p_c * t)
                         - N_ia * n_ia
                               * (2 * std::pow(p_c, 2) * t * (N_ia + t * inactive_segment_rate)
                                      * std::exp(-N_ia * p_c * t)
                                  - 2 * p_c * (t / time_step_size + 1) * std::exp(-N_ia * p_c * t)
                                  - std::pow(p_s, 2) * t * (N_ia + t * inactive_segment_rate)
                                        * std::exp(-N_ia * p_s * t)
                                  + p_s * (t / time_step_size + 1) * std::exp(-N_ia * p_s * t))
                         - 2 * n_a * p_s * (N_a + t * active_segment_rate) * std::exp(-N_a * p_s * t)
                         - 2 * n_ia * p_c * (N_ia + t * inactive_segment_rate)
                               * std::exp(-N_ia * p_c * t)
                         - n_ia
                               * (-2 * p_c * (N_ia + t * inactive_segment_rate)
                                      * std::exp(-N_ia * p_c * t)
                                  + p_s * (N_ia + t * inactive_segment_rate)
                                        * std::exp(-N_ia * p_s * t)))
                      / n_ia,
            0,
            0,
            0,
            gamma * time_step_size
                * (-2 * std::pow(p_c, 2) * t * (N_a + t * active_segment_rate)
                       * std::exp(-N_a * p_c * t)
                   + 2 * p_c * (t / time_step_size + 1) * std::exp(-N_a * p_c * t)
                   - std::pow(p_s, 2) * t * (N_a + t * active_segment_rate) * std::exp(-N_a * p_s * t)
                   + p_s * (t / time_step_size + 1) * std::exp(-N_a * p_s * t)),
            0,
            time_step_size
                    * (2 * p_c * (N_a + t * active_segment_rate) * std::exp(-N_a * p_c * t)
                       + p_s * (N_a + t * active_segment_rate) * std::exp(-N_a * p_s * t))
                + 1)
        .finished();
}

inline auto failure_density(double const failure_rate,
                            double const segment,
                            double const segment_rate,
                            double const current_time) -> double
{
    return failure_rate * (segment + current_time * segment_rate)
           * std::exp(-failure_rate * segment * current_time);
}

inline auto compute_creation_rate(double const n_a,
                                  double const n_ia,
                                  double const alpha,
                                  double const eta) -> double
{
    return alpha * n_ia + 4 * eta * n_a;
}

inline auto update_network_parameters(vector5 const& values,
                                      double scission_rate,
                                      double crosslink_rate,
                                      double time_step_size) noexcept(false) -> vector5
{
    auto constexpr increment_tolerance = 1.0e-8;
    auto constexpr residual_tolerance = 1.0e-8;

    auto constexpr maximum_iterations = 20;

    auto initial_residual_norm = 1.0;

    vector5 current_values = values;

    auto is_not_converged = false;

    auto iteration = 0;

    do
    {
        vector5 const residual = assemble_residual(current_values,
                                                   values,
                                                   scission_rate,
                                                   crosslink_rate,
                                                   time_step_size,
                                                   time_step_size);
        matrix5 const jacobian = assemble_jacobian(current_values,
                                                   values,
                                                   scission_rate,
                                                   crosslink_rate,
                                                   time_step_size,
                                                   time_step_size);

        vector5 const solution_increment = jacobian.partialPivLu().solve(-residual);

        current_values += solution_increment;

        if (iteration == 0)
        {
            initial_residual_norm = residual.norm();
        }

        is_not_converged = residual.norm() / initial_residual_norm > residual_tolerance
                           || solution_increment.norm() / current_values.norm() > increment_tolerance;

    } while (is_not_converged && iteration++ < maximum_iterations);

    if (iteration == maximum_iterations)
    {
        throw std::runtime_error("Maximum number of Newton-Raphson iterations reached during "
                                 "network dynamics time-stepping algorithm");
    }
    return current_values;
}
