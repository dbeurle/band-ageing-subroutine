
#pragma once

#include <Eigen/Dense>

using vector = Eigen::VectorXd;

/// Compile time column vector of size 5
using vector5 = Eigen::Matrix<double, 5, 1>;
/// Compile time column vector of size 6
using vector6 = Eigen::Matrix<double, 6, 1>;

// Compile time 3x3 row major matrix type
using matrix3 = Eigen::Matrix<double, 3, 3, Eigen::RowMajor>;
// Compile time 5x5 row major matrix type
using matrix5 = Eigen::Matrix<double, 5, 5, Eigen::RowMajor>;
// Compile time 6x6 row major matrix type
using matrix6 = Eigen::Matrix<double, 6, 6, Eigen::RowMajor>;
// Compile time 6x9 row major matrix type
using matrix69 = Eigen::Matrix<double, 6, 9, Eigen::RowMajor>;

template <typename StorageType>
using view = Eigen::Map<StorageType>;
