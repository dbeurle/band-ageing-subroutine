
#define CATCH_CONFIG_MAIN

#include <catch2/catch.hpp>

#include "network_model.hpp"
#include "tensor.hpp"
#include "eval.hpp"

TEST_CASE("Failure density")
{
    //
    REQUIRE(failure_density(1.0e-5, 60, 0.01, 0) == Approx(6.0e-4));
}

TEST_CASE("Creation rate")
{
    REQUIRE(compute_creation_rate(1.0e6, 1.0e4, 0.01, 0.01) == Approx(40100));
}

TEST_CASE("Implicit time integration")
{
    vector5 previous_values;

    previous_values << 10.0, 0.014, 60.2, 59.8, 0.98;

    auto const scission_rate = 1e-4;
    auto const crosslink_rate = 1e-5;
    auto const time = 0.1;
    auto const time_step_size = 0.2;

    auto const current_values = update_network_parameters(previous_values,
                                                          scission_rate,
                                                          crosslink_rate,
                                                          time_step_size);

    REQUIRE(current_values(4) < 0.98);

    SECTION("Residual assembly")
    {
        auto const residual = assemble_residual(current_values,
                                                previous_values,
                                                scission_rate,
                                                crosslink_rate,
                                                time_step_size,
                                                time_step_size);

        REQUIRE(residual.norm() == Approx(0.0).margin(1.0e-5));
    }

    SECTION("Jacobian assembly")
    {
        Eigen::EigenSolver<matrix5> eigensolver(assemble_jacobian(current_values,
                                                                  previous_values,
                                                                  scission_rate,
                                                                  crosslink_rate,
                                                                  time_step_size,
                                                                  time_step_size));
        if (eigensolver.info() != Eigen::Success)
        {
            throw std::runtime_error("Error in eigenvalue solver");
        }
        REQUIRE(eigensolver.eigenvalues().real().minCoeff() > 0);
    }
}

TEST_CASE("Voigt mapping")
{
    REQUIRE(voigt_map(0, 0) == 0);
    REQUIRE(voigt_map(1, 1) == 1);
    REQUIRE(voigt_map(2, 2) == 2);
    REQUIRE(voigt_map(1, 2) == 3);
    REQUIRE(voigt_map(0, 2) == 4);
    REQUIRE(voigt_map(0, 1) == 5);
    REQUIRE(voigt_map(1, 0) == 5);
    REQUIRE(voigt_map(2, 0) == 4);
    REQUIRE(voigt_map(2, 1) == 3);
}

TEST_CASE("Material tangent pull back")
{
    matrix3 deformation_gradient(3, 3);
    deformation_gradient << 1.5, 0.0, 0.0, //
        0.0, 1.0 / std::sqrt(1.5), 0.0,    //
        0.0, 0.0, 1.0 / std::sqrt(1.5);

    matrix3 const C_inv = (deformation_gradient.transpose() * deformation_gradient).inverse();

    auto const lambda = 2.0;
    auto const mu = 1.0;

    matrix6 spatial_gradient = matrix6::Zero();
    spatial_gradient << lambda + 2 * mu, lambda, lambda, 0.0, 0.0, 0.0, //
        lambda, lambda + 2 * mu, lambda, 0.0, 0.0, 0.0,                 //
        lambda, lambda, lambda + 2 * mu, 0.0, 0.0, 0.0,                 //
        0.0, 0.0, 0.0, mu, 0.0, 0.0,                                    //
        0.0, 0.0, 0.0, 0.0, mu, 0.0,                                    //
        0.0, 0.0, 0.0, 0.0, 0.0, mu;                                    //

    matrix6 material_gradient = matrix6::Zero();
    material_gradient << lambda * std::pow(C_inv(0, 0), 2) + 2 * mu * std::pow(C_inv(0, 0), 2),
        lambda * C_inv(0, 0) * C_inv(1, 1)
            + mu * (std::pow(C_inv(0, 1), 2) + C_inv(0, 1) * C_inv(1, 0)),
        lambda * C_inv(0, 0) * C_inv(2, 2)
            + mu * (std::pow(C_inv(0, 2), 2) + C_inv(0, 2) * C_inv(2, 0)),
        lambda * C_inv(0, 0) * C_inv(1, 2)
            + mu * (C_inv(0, 1) * C_inv(0, 2) + C_inv(0, 2) * C_inv(1, 0)),
        lambda * C_inv(0, 0) * C_inv(0, 2) + 2 * mu * C_inv(0, 0) * C_inv(0, 2),
        lambda * C_inv(0, 0) * C_inv(0, 1) + 2 * mu * C_inv(0, 0) * C_inv(0, 1),
        lambda * C_inv(0, 0) * C_inv(1, 1)
            + mu * (C_inv(0, 1) * C_inv(1, 0) + std::pow(C_inv(1, 0), 2)),
        lambda * std::pow(C_inv(1, 1), 2) + 2 * mu * std::pow(C_inv(1, 1), 2),
        lambda * C_inv(1, 1) * C_inv(2, 2)
            + mu * (std::pow(C_inv(1, 2), 2) + C_inv(1, 2) * C_inv(2, 1)),
        lambda * C_inv(1, 1) * C_inv(1, 2) + 2 * mu * C_inv(1, 1) * C_inv(1, 2),
        lambda * C_inv(0, 2) * C_inv(1, 1)
            + mu * (C_inv(0, 1) * C_inv(1, 2) + C_inv(1, 0) * C_inv(1, 2)),
        lambda * C_inv(0, 1) * C_inv(1, 1)
            + mu * (C_inv(0, 1) * C_inv(1, 1) + C_inv(1, 0) * C_inv(1, 1)),
        lambda * C_inv(0, 0) * C_inv(2, 2)
            + mu * (C_inv(0, 2) * C_inv(2, 0) + std::pow(C_inv(2, 0), 2)),
        lambda * C_inv(1, 1) * C_inv(2, 2)
            + mu * (C_inv(1, 2) * C_inv(2, 1) + std::pow(C_inv(2, 1), 2)),
        lambda * std::pow(C_inv(2, 2), 2) + 2 * mu * std::pow(C_inv(2, 2), 2),
        lambda * C_inv(1, 2) * C_inv(2, 2)
            + mu * (C_inv(1, 2) * C_inv(2, 2) + C_inv(2, 1) * C_inv(2, 2)),
        lambda * C_inv(0, 2) * C_inv(2, 2)
            + mu * (C_inv(0, 2) * C_inv(2, 2) + C_inv(2, 0) * C_inv(2, 2)),
        lambda * C_inv(0, 1) * C_inv(2, 2)
            + mu * (C_inv(0, 2) * C_inv(2, 1) + C_inv(2, 0) * C_inv(2, 1)),
        lambda * C_inv(0, 0) * C_inv(1, 2)
            + mu * (C_inv(0, 2) * C_inv(1, 0) + C_inv(1, 0) * C_inv(2, 0)),
        lambda * C_inv(1, 1) * C_inv(1, 2)
            + mu * (C_inv(1, 1) * C_inv(1, 2) + C_inv(1, 1) * C_inv(2, 1)),
        lambda * C_inv(1, 2) * C_inv(2, 2) + 2 * mu * C_inv(1, 2) * C_inv(2, 2),
        lambda * std::pow(C_inv(1, 2), 2)
            + mu * (C_inv(1, 1) * C_inv(2, 2) + std::pow(C_inv(1, 2), 2)),
        lambda * C_inv(0, 2) * C_inv(1, 2)
            + mu * (C_inv(0, 2) * C_inv(1, 2) + C_inv(1, 0) * C_inv(2, 2)),
        lambda * C_inv(0, 1) * C_inv(1, 2)
            + mu * (C_inv(0, 2) * C_inv(1, 1) + C_inv(1, 0) * C_inv(2, 1)),
        lambda * C_inv(0, 0) * C_inv(0, 2)
            + mu * (C_inv(0, 0) * C_inv(0, 2) + C_inv(0, 0) * C_inv(2, 0)),
        lambda * C_inv(0, 2) * C_inv(1, 1)
            + mu * (C_inv(0, 1) * C_inv(1, 2) + C_inv(0, 1) * C_inv(2, 1)),
        lambda * C_inv(0, 2) * C_inv(2, 2) + 2 * mu * C_inv(0, 2) * C_inv(2, 2),
        lambda * C_inv(0, 2) * C_inv(1, 2)
            + mu * (C_inv(0, 1) * C_inv(2, 2) + C_inv(0, 2) * C_inv(1, 2)),
        lambda * std::pow(C_inv(0, 2), 2)
            + mu * (C_inv(0, 0) * C_inv(2, 2) + std::pow(C_inv(0, 2), 2)),
        lambda * C_inv(0, 1) * C_inv(0, 2)
            + mu * (C_inv(0, 0) * C_inv(2, 1) + C_inv(0, 1) * C_inv(0, 2)),
        lambda * C_inv(0, 0) * C_inv(0, 1)
            + mu * (C_inv(0, 0) * C_inv(0, 1) + C_inv(0, 0) * C_inv(1, 0)),
        lambda * C_inv(0, 1) * C_inv(1, 1) + 2 * mu * C_inv(0, 1) * C_inv(1, 1),
        lambda * C_inv(0, 1) * C_inv(2, 2)
            + mu * (C_inv(0, 2) * C_inv(1, 2) + C_inv(0, 2) * C_inv(2, 1)),
        lambda * C_inv(0, 1) * C_inv(1, 2)
            + mu * (C_inv(0, 1) * C_inv(1, 2) + C_inv(0, 2) * C_inv(1, 1)),
        lambda * C_inv(0, 1) * C_inv(0, 2)
            + mu * (C_inv(0, 0) * C_inv(1, 2) + C_inv(0, 1) * C_inv(0, 2)),
        lambda * std::pow(C_inv(0, 1), 2)
            + mu * (C_inv(0, 0) * C_inv(1, 1) + std::pow(C_inv(0, 1), 2));

    SECTION("Symmetry")
    {
        REQUIRE((spatial_gradient - spatial_gradient.transpose()).norm() == Approx(0.0).margin(1e-5));
        REQUIRE((material_gradient - material_gradient.transpose()).norm()
                == Approx(0.0).margin(1e-5));
    }
    SECTION("Eigenvalues for spatial gradient operator")
    {
        Eigen::SelfAdjointEigenSolver<matrix6> eigensolver(spatial_gradient);
        REQUIRE(eigensolver.eigenvalues().minCoeff() > 0);
    }
    SECTION("Eigenvalues for spatial gradient operator")
    {
        Eigen::SelfAdjointEigenSolver<matrix6> eigensolver(material_gradient);
        REQUIRE(eigensolver.eigenvalues().minCoeff() > 0);
    }
    SECTION("Pull back operator")
    {
        auto const test_material_tangent = pull_back_material_tangent(deformation_gradient,
                                                                      spatial_gradient);

        Eigen::SelfAdjointEigenSolver<matrix6> eigensolver(test_material_tangent);
        REQUIRE(eigensolver.eigenvalues().minCoeff() > 0);

        std::cout << "spatial_gradient:\n" << spatial_gradient << '\n';
        std::cout << "material_gradient:\n" << material_gradient << '\n';
        std::cout << "test_material_gradient:\n" << test_material_tangent << '\n';

        REQUIRE((material_gradient - test_material_tangent).norm() == Approx(0.0).margin(1e-8));
    }
}

TEST_CASE("Full test")
{
    double F_old[3][3] = {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}};

    auto static_parameter_size = 3;
    std::array<double, 3> static_parameters = {1.0e-5, 1.0e-5, 10000};

    auto network_parameter_size = 5;
    std::array<double, 5> network_parameters = {1000, 0.1, 60.0, 60.0, 1.0};

    auto time_step_size = 0.0;

    auto pk2_stress_size = 6;
    double pk2_stress[6] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0};

    double pk2_secondary_stress[6] = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0};

    double jacobian[6][9] = {{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
                             {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
                             {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
                             {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
                             {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0},
                             {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0}};

    for (int iteration = 0; iteration < 4; ++iteration)
    {
        auto const stretch = 1.0 + iteration * 0.1;

        double F[3][3] = {{stretch, 0, 0},
                          {0, 1 / std::sqrt(stretch), 0},
                          {0, 0, 1 / std::sqrt(stretch)}};

        eval(F_old,
             F,
             nullptr,
             nullptr,
             nullptr,
             &time_step_size,
             pk2_stress,
             jacobian,
             &static_parameter_size,
             static_parameters.data(),
             &network_parameter_size,
             network_parameters.data(),
             &pk2_stress_size,
             pk2_secondary_stress,
             nullptr);

        std::cout << "network_parameters\n";
        for (auto i : network_parameters)
        {
            std::cout << i << '\n';
        }
        std::cout << '\n';

        std::cout << "pk2_stress\n";
        for (auto i : pk2_stress)
        {
            std::cout << i << '\n';
        }
        std::cout << '\n';

        std::cout << "pk2_secondary_stress\n";
        for (auto i : pk2_secondary_stress)
        {
            std::cout << i << '\n';
        }
        std::cout << '\n';

        std::cout << "jacobian\n";

        for (auto i = 0; i < 6; ++i)
        {
            for (auto j = 0; j < 9; ++j)
            {
                std::cout << jacobian[i][j] << ',';
            }
            std::cout << '\n';
        }
        std::cout << '\n';
    }
}

/*
TEST_CASE("")
{
    double el[6] = {1.0, 0.0, 0.0, 0.0, 0.0, 0.0};
    double par[2] = {2.0, 5.0};

    double Sl[6];
    double jac[6][6];

    auto size = 2;

    eval_strain(&el[0],     // Green-Lagrange strain, input
                &Sl[0],     // Second Piola-Kirchhoff stress, output
                &jac[0][0], // Jacobian of stress with respect to strain,output
                &size,      // Number of material model parameters, input
                par,
                nullptr,
                nullptr,
                nullptr);

    std::cout << "Jacobian\n";
    for (int i = 0; i < 6; ++i)
    {
        for (int j = 0; j < 6; ++j)
        {
            std::cout << jac[i][j] << ',';
        }
        std::cout << '\n';
    }

    std::cout << "Stress\n";
    for (auto i : Sl)
    {
        std::cout << i << ',';
    }
}
*/
