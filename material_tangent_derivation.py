import sympy
from sympy.functions.special.tensor_functions import KroneckerDelta


def index_to_voigt(i, j):

    if i == j:
        return i

    if i > j:
        j, i = i, j

    if i == 0:
        if j == 2:
            return 4
        if j == 1:
            return 5
    return 3


def compute_full_tangent_SE(lamb_0, mu, rcg_inv):

    C_SE = sympy.MutableDenseNDimArray([0 for i in range(81)], (3, 3, 3, 3))

    for i in range(3):
        for j in range(3):
            for k in range(3):
                for l in range(3):
                    C_SE[i, j, k, l] = lamb_0 * rcg_inv[i, j] * rcg_inv[k, l] + mu * (
                        rcg_inv[i, k] * rcg_inv[j, l] + rcg_inv[i, l] * rcg_inv[k, j]
                    )
    return C_SE


def compute_full_tangent_TK(lamb_0, mu):
    C_TK = sympy.MutableDenseNDimArray([0 for _ in range(81)], (3, 3, 3, 3))

    for i in range(3):
        for j in range(3):
            for k in range(3):
                for l in range(3):
                    C_TK[i, j, k, l] = lamb_0 * KroneckerDelta(i, j) * KroneckerDelta(
                        k, l
                    ) + mu * (
                        KroneckerDelta(i, k) * KroneckerDelta(j, l)
                        + KroneckerDelta(i, l) * KroneckerDelta(k, j)
                    )
    return C_TK


def convert_to_voigt(C_sym_full, voigt_map):

    C_v = sympy.zeros(6, 6)
    for I, (i, j) in enumerate(voigt_map):
        for J, (k, l) in enumerate(voigt_map):
            C_v[I, J] = C_sym_full[i, j, k, l]
    return C_v


def convert_to_silly_voigt(C_sym_full, voigt_map, full_voigt_map):

    C_v = sympy.zeros(len(voigt_map), len(full_voigt_map))

    for I, (i, j) in enumerate(voigt_map):
        for J, (k, l) in enumerate(full_voigt_map):
            C_v[I, J] = C_sym_full[i, j, k, l]

    return C_v


def compute_pushforward(C_SE, F):
    """ Pull back of the material tangent of Truesdell rate of the Kirchhoff stress"""
    C_TK = sympy.MutableDenseNDimArray([0 for _ in range(81)], (3, 3, 3, 3))

    for i in range(3):
        for j in range(3):
            for k in range(3):
                for l in range(3):
                    for m in range(3):
                        for n in range(3):
                            for p in range(3):
                                for q in range(3):
                                    C_TK[m, n, p, q] += (
                                        F[m, i]
                                        * F[n, j]
                                        * F[p, k]
                                        * F[q, l]
                                        * C_SE[i, j, k, l]
                                    )
    return C_TK


def compute_pullback(C_TK, F_inv):
    """ Pull back of the material tangent of Truesdell rate of the Kirchhoff stress"""
    C_SE = sympy.MutableDenseNDimArray([0 for _ in range(81)], (3, 3, 3, 3))

    for i in range(3):
        for j in range(3):
            for k in range(3):
                for l in range(3):
                    for m in range(3):
                        for n in range(3):
                            for p in range(3):
                                for q in range(3):
                                    C_SE[i, j, k, l] += (
                                        F_inv[i, m]
                                        * F_inv[j, n]
                                        * F_inv[k, p]
                                        * F_inv[l, q]
                                        * C_TK[m, n, p, q]
                                    )
    return C_SE


def main():

    # Deformation
    stretch = sympy.Symbol("lambda")

    # Material parameters
    lamb_0 = sympy.Symbol("lambda_0", real=True, positive=True)
    mu = sympy.Symbol("mu_0", real=True, positive=True)

    # Diagonal
    F = sympy.diag(1, 1, 1)

    # Uniaxial stretch
    F = sympy.diag(stretch, 1 / sympy.sqrt(stretch), 1 / sympy.sqrt(stretch))

    # Pure shear
    F = sympy.eye(3)
    F[0, 1] = stretch

    # Generic deformation
    F = sympy.MatrixSymbol("F", 3, 3)

    # Create a random matrix (ensure it's admissible!)
    # F = sympy.randMatrix(3, 3)

    F_inv = sympy.Inverse(F).doit()

    rcg = sympy.transpose(F) * F
    rcg_inv = sympy.Inverse(rcg).doit()

    print("Deformation gradient")
    sympy.pprint(F)

    print("Inverse deformation gradient")
    sympy.pprint(F_inv)

    voigt_map = [(0, 0), (1, 1), (2, 2), (1, 2), (0, 2), (0, 1)]

    # Along rows
    full_voigt_map = [
        (0, 0),
        (0, 1),
        (0, 2),
        (1, 0),
        (1, 1),
        (1, 2),
        (2, 0),
        (2, 1),
        (2, 2),
    ]
    # Along columns for Comsol
    full_voigt_map = [
        (0, 0),
        (1, 0),
        (2, 0),
        (0, 1),
        (1, 1),
        (2, 1),
        (0, 2),
        (1, 2),
        (2, 2),
    ]

    C_symbolic = sympy.MutableDenseNDimArray(
        [
            sympy.Symbol(
                "C_SE("
                + str(index_to_voigt(i, j))
                + ","
                + str(index_to_voigt(k, l))
                + ")"
            )
            for i in range(3)
            for j in range(3)
            for k in range(3)
            for l in range(3)
        ],
        (3, 3, 3, 3),
    )

    # Compute C_SF in full tensor notation
    C_SF = sympy.MutableDenseNDimArray([0 for i in range(81)], (3, 3, 3, 3))
    for i in range(3):
        for j in range(3):
            for k in range(3):
                for l in range(3):
                    for m in range(3):
                        C_SF[i, j, k, l] += C_symbolic[i, j, k, m] * F[l, m]

    C_SE = compute_full_tangent_SE(lamb_0, mu, rcg_inv)
    C_TK = compute_full_tangent_TK(lamb_0, mu)

    C_SE_v = convert_to_voigt(C_SE, voigt_map)
    C_TK_v = convert_to_voigt(C_TK, voigt_map)
    C_SF_v = convert_to_silly_voigt(C_SF, voigt_map, full_voigt_map)

    C_SE_pullback = compute_pullback(C_TK, F_inv)
    C_SE_v_pullback = convert_to_voigt(C_SE_pullback, voigt_map)

    # print(convert_to_voigt(compute_pullback(C_symbolic, F_inv), voigt_map))
    print(C_SF_v)

    exit()

    # Correct for Voigt notation
    C_SE_book = sympy.zeros(6, 6)
    for I, (i, j) in enumerate(voigt_map):
        for J, (k, l) in enumerate(voigt_map):
            C_SE_book[I, J] = lamb_0 * rcg_inv[i, j] * rcg_inv[k, l] + mu * (
                rcg_inv[i, k] * rcg_inv[j, l] + rcg_inv[i, l] * rcg_inv[k, j]
            )

    sympy.pprint(C_TK_v)

    print("Truesdell voigt test")
    sympy.pprint(
        C_TK_v
        - sympy.simplify(convert_to_voigt(compute_pushforward(C_SE, F), voigt_map))
    )

    # expr = C_SE - compute_pullback(compute_pushforward(C_SE, F), F_inv)

    # expr1 = C_SE_pullback - C_SE

    # print("Alex's crazy idea")
    # sympy.pprint(sympy.simplify(expr))

    # print("Full tensor zero check")
    # for i in range(3):
    #     for j in range(3):
    #         for k in range(3):
    #             for l in range(3):
    #                 sympy.pprint(sympy.simplify(sympy.expand(expr1[i, j, k, l])))
    # sympy.pprint(sympy.simplify(sympy.expand(expr1[0, 0, 0, 0])))

    print("Voigt zero check")
    sympy.pprint(sympy.simplify(C_SE_book - C_SE_v_pullback))

    # sympy.pprint(C_SE_book)
    # print(sympy.simplify(C_SF_v))


if __name__ == "__main__":
    main()
