
#include "matrix_types.hpp"
#include "network_model.hpp"
#include "tensor.hpp"
#include "eval.hpp"

#include <cmath>

namespace
{
inline auto volumetric_free_energy_dJ(double const deformation_gradient_determinant,
                                      double const bulk_modulus) noexcept -> double
{
    auto const J = deformation_gradient_determinant;
    return bulk_modulus / 2.0 * (J - 1.0 / J);
}

inline auto volumetric_free_energy_second_d2J(double const deformation_gradient_determinant,
                                              double const bulk_modulus) noexcept -> double
{
    auto const J = deformation_gradient_determinant;
    return bulk_modulus / 2.0 * (1.0 + 1.0 / std::pow(J, 2));
}

/// Compute the macro stress of the primary network
inline auto compute_initial_macro_stress(double const active_shear_modulus, matrix3 const& F_bar)
    -> matrix3
{
    return active_shear_modulus * F_bar * F_bar.transpose();
}

/// Compute the stress on the intermediate configuration defined as the rest state
/// when the chains were built. This is simply the deviatoric contribution
/// from a Neo-Hooke model defined in the reference configuration
inline auto compute_intermediate_macro_stress(double const shear_modulus,
                                              double const reduction_factor) -> matrix3
{
    return shear_modulus / reduction_factor * matrix3::Identity();
}

inline auto compute_kirchhoff_stress(double const pressure, matrix3 const& macro_stress) -> matrix3
{
    return pressure * matrix3::Identity() + deviatoric(macro_stress);
}

/// Compute the Truesdell rate of the Kirchhoff stress material tangent
inline auto compute_material_tangent(double const deformation_gradient_determinant,
                                     double const bulk_modulus,
                                     matrix3 const& macro_stress) -> matrix6
{
    auto const J = deformation_gradient_determinant;

    auto const pressure = J * volumetric_free_energy_dJ(J, bulk_modulus);
    auto const kappa = std::pow(J, 2) * volumetric_free_energy_second_d2J(J, bulk_modulus);

    // Outer product
    matrix6 const IoI = voigt::I_outer_I();

    // Fourth order identity
    matrix6 const I = voigt::kinematic::fourth_order_identity();

    // Deviatoric fourth order tensor
    matrix6 const P = voigt::kinetic::deviatoric();

    // clang-format off
    matrix6 const D = 2.0 / 3.0 * (macro_stress.trace() * I -
                     (outer_product(macro_stress, matrix3::Identity()) +
                                   outer_product(matrix3::Identity(), macro_stress)));

    // clang-format on
    return (kappa + pressure) * IoI - 2.0 * pressure * I + P * D * P;
}

inline auto voigt_to_tensor(double* values) -> matrix3
{
    matrix3 t;

    t(0, 0) = values[0];
    t(1, 1) = values[1];
    t(2, 2) = values[2];
    t(1, 2) = t(2, 1) = values[3];
    t(0, 2) = t(2, 0) = values[4];
    t(0, 1) = t(1, 0) = values[5];

    return t;
}
}

extern "C" {
/// Entry point for COMSOL
EXPORT int eval(
    double FlOld[3][3],
    double Fl[3][3],
    double*,          // tempOld,
    double*,          // temp,
    double*,          // sysT[1][9],
    double* delta,    // Time step size
    double Sl[6],     // S11, S22, S33, S23, S13, S12
    double Jac[6][9], // Material tangent
    int* nPar,        // 3
    double* par,      // scission_rate (1.0e-5), crosslink_rate (1.0e-5), bulk_modulus (10MPa)
    int* network_parameters_size,  // 5
    double* network_parameters_io, // active_shear_modulus (1MPa), inactive_shear_modulus (1kPa),
                                   // active_segments (60), inactive_segments (60), reduction_factor (1.0)
    int* pk2_stress_size,            // 6
    double* pk2_stress_intermediate, // S11, S22, S33, S23, S13, S12 (null @ t = 0)
    char*)
{
    if (*nPar != 3)
    {
        return 1;
    }
    if (*network_parameters_size != 5 || *pk2_stress_size != 6)
    {
        return 2;
    }

    auto const time_step_size = *delta;

    // Bind to C++ types
    auto deformation_gradient = view<matrix3 const>(*Fl, 3, 3);
    auto material_parameters = view<vector const>(par, *nPar);
    auto network_parameters_old = view<vector const>(network_parameters_io, *network_parameters_size);

    // 'Constant' material properties
    auto const scission_rate = material_parameters(0);
    auto const crosslink_rate = material_parameters(1);
    auto const bulk_modulus = material_parameters(2);

    // Integrate the network dynamics
    vector5 network_parameters = time_step_size == 0.0
                                     ? network_parameters_old
                                     : update_network_parameters(network_parameters_old,
                                                                 scission_rate,
                                                                 crosslink_rate,
                                                                 time_step_size);

    // Network parameters
    auto const active_shear_modulus = network_parameters(0);
    auto const inactive_shear_modulus = network_parameters(1);
    auto const active_segments = network_parameters(2);
    auto const inactive_segments = network_parameters(3);
    auto const reduction_factor = network_parameters(4);

    auto const active_segment_rate = time_step_size == 0.0
                                         ? 0.0
                                         : (active_segments - network_parameters_old(2))
                                               / time_step_size;
    auto const inactive_segment_rate = time_step_size == 0.0
                                           ? 0.0
                                           : (inactive_segments - network_parameters_old(3))
                                                 / time_step_size;

    auto const alpha = failure_density(crosslink_rate,
                                       inactive_segments,
                                       inactive_segment_rate,
                                       time_step_size);
    auto const eta = failure_density(crosslink_rate,
                                     active_segments,
                                     active_segment_rate,
                                     time_step_size);

    std::copy_n(network_parameters.data(), network_parameters.size(), network_parameters_io);

    matrix3 secondary_macro_stress = voigt_to_tensor(pk2_stress_intermediate);

    auto const creation_rate = compute_creation_rate(active_shear_modulus,
                                                     inactive_shear_modulus,
                                                     alpha,
                                                     eta);

    matrix3 const F_bar = unimodular(deformation_gradient);

    matrix3 const primary_macro_stress = compute_initial_macro_stress(active_shear_modulus, F_bar);

    // Perform an implicit Euler update of the secondary macro-PK2 stress
    secondary_macro_stress += pull_back_contravariant(F_bar,
                                                      compute_intermediate_macro_stress(creation_rate
                                                                                            * time_step_size,
                                                                                        reduction_factor));

    // Update the history variable
    pk2_stress_intermediate[0] = secondary_macro_stress(0, 0);
    pk2_stress_intermediate[1] = secondary_macro_stress(1, 1);
    pk2_stress_intermediate[2] = secondary_macro_stress(2, 2);
    pk2_stress_intermediate[3] = secondary_macro_stress(1, 2);
    pk2_stress_intermediate[4] = secondary_macro_stress(0, 2);
    pk2_stress_intermediate[5] = secondary_macro_stress(0, 1);

    matrix3 const macro_stress = reduction_factor
                                 * (primary_macro_stress
                                    + push_forward_contravariant(F_bar, secondary_macro_stress));

    auto const J = deformation_gradient.determinant();

    auto const pressure = J * volumetric_free_energy_dJ(J, bulk_modulus);

    // Pull back the stress to get a PK2 stress and pass back to COMSOL
    matrix3 const pk2_stress = pull_back_contravariant(deformation_gradient,
                                                       compute_kirchhoff_stress(pressure,
                                                                                macro_stress));

    // Truesdell rate of the Kirchhoff stress material tangent
    matrix6 const spatial_tangent_operator = compute_material_tangent(J, bulk_modulus, macro_stress);

    // Pull back the material tangent operator and convert to the 6x9
    matrix6 const tangent_operator = pull_back_material_tangent(deformation_gradient,
                                                                spatial_tangent_operator);

    matrix69 const odd_tangent_operator = convert_material_tangent(tangent_operator,
                                                                   deformation_gradient);

    // Update the second Piola-Kirchhoff stress
    Sl[0] = pk2_stress(0, 0);
    Sl[1] = pk2_stress(1, 1);
    Sl[2] = pk2_stress(2, 2);
    Sl[3] = pk2_stress(1, 2);
    Sl[4] = pk2_stress(0, 2);
    Sl[5] = pk2_stress(0, 1);

    // Copy across the tangent operator for Comsol
    std::copy_n(odd_tangent_operator.data(), 6 * 9, &(Jac[0][0]));

    return 0;
}
}

// if (false)
// {
//     std::ofstream ostrm("model_output.txt");
//
//     ostrm << "time_step_size " << time_step_size << "\n"
//           << "active_segment_rate " << active_segment_rate << "\n"
//           << "inactive_segment_rate " << inactive_segment_rate << "\n"
//           << "network parameters\n"
//           << network_parameters << "\n"
//           << "alpha " << alpha << "\n"
//           << "eta " << eta << "\n\n";
//
//     ostrm << "pressure\n"
//           << pressure << "\n"
//           << "creation_rate\n"
//           << creation_rate << "\n"
//           << "F_bar\n"
//           << F_bar << "\n"
//           << "Jacobian determinant\n"
//           << J << "\n"
//           << "primary_macro_stress\n"
//           << primary_macro_stress << "\n"
//           << "secondary_macro_stress\n"
//           << secondary_macro_stress << "\n"
//           << "macro_stress\n"
//           << macro_stress << "\n"
//           << "pk2_stress\n"
//           << pk2_stress << "\n"
//           << "spatial_tangent_operator\n"
//           << spatial_tangent_operator << "\n"
//           << "tangent_operator\n"
//           << tangent_operator << "\n"
//           << "odd_tangent_operator\n"
//           << odd_tangent_operator << "\n";
// }
