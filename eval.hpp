
#pragma once

#ifdef _MSC_VER
#define EXPORT __declspec(dllexport)
#else
#define EXPORT
#endif

extern "C" {
EXPORT int eval(
    double FlOld[3][3],
    double Fl[3][3],
    double*,          // tempOld,
    double*,          // temp,
    double*,          // sysT[1][9],
    double* delta,    // Time step size
    double Sl[6],     // S11, S22, S33, S23, S13, S12
    double Jac[6][9], // Material tangent
    int* nPar,        // 3
    double* par,      // scission_rate (1.0e-5), crosslink_rate (1.0e-5), bulk_modulus (10MPa)
    int* network_parameters_size,  // 5
    double* network_parameters_io, // active_shear_modulus (1MPa), inactive_shear_modulus (1kPa),
                                   // active_segments (60), inactive_segments (60), reduction_factor (1.0)
    int* pk2_stress_size,            // 6
    double* pk2_stress_intermediate, // S11, S22, S33, S23, S13, S12 (null @ t = 0)
    char*);

EXPORT int eval_strain(double* el,  // Green-Lagrange strain, input
                       double* Sl,  // Second Piola-Kirchhoff stress, output
                       double* Jac, // Jacobian of stress with respect to strain,output
                       int* nPar,   // Number of material model parameters, input
                       double* par,
                       int(*nStates1),
                       double* states1,
                       char* errMsg);
}
