# BAND oxidative ageing model subroutine

An implementation of the BAND oxidative ageing model for COMSOL.

This code is in support of an upcoming publication. For reproducability and quality purposes it includes a test suite outside COMSOL. The code is written in C++ and called through a C interface for compatibility with COMSOL.

## Compilation steps

The `Eigen` linear algebra library, the test suite library `Catch2`, and a modern C++14 compiler is required. Configuring and building is handled directly with `CMake`.

Parallelism of the subroutine is available through Eigen and is enabled through CMake with additional compiler flags. This will be specific to your CPU architecture. Note that this subroutine is threadsafe and does not require any initialisation or teardown steps.

## Acknowledgements

This work has been funded by the German Research Council (DFG) under the IRTG1627 programme. The theory has been developed by myself in conjunction with Markus André, Udo Nackenhorst and Rodrigue Desmorat. COMSOL support and nasty pull back operations validation has been performed collaboratively with Alexander Ricker and Nils Kröger of the DIK e.V Hannover, and extensions to this work to include oxygen reaction-diffusion is being performed by Marlis Reiber with the support of the DIK.

## Contributions

Any problems or questions? Please open an issue.

If ready-made binaries are required, this can be arranged if the desired architecture is given.